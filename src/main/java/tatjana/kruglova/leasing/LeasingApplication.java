package tatjana.kruglova.leasing;

import org.openqa.selenium.By;

public class LeasingApplication {

    public static final By INPUT_VEHICLE_PRICE = By.id("origin-price");
    public static final By INPUT_DOWNPAYMENT_PERCENTAGE = By.id("initial_percentage");
    public static final By INPUT_DOWNPAYMENT_SUM = By.id("initial");
    public static final By INPUT_RESIDUAL_VALUE_PERCENTAGE = By.id("reminder_percentage");
    public static final By INPUT_RESIDUAL_VALUE_SUM = By.id("reminder");
    public static final By RADIO_BUTTON_PRIVATE_PERSON = By.id("account_type-P");
    public static final By RADIO_BUTTON_LEGAL_PERSON = By.id("account_type-C");
    public static final By RADIO_BUTTON_FINANTIAL_LEASE = By.id("lease_type-HP");
    public static final By RADIO_BUTTON_OPERATING_LEASE = By.id("lease_type-FL");
    public static final By CHECKBOX_VAT_VALUE = By.id("vat_included");
    public static final By SELECT_PERIOD_YEARS = By.id("duration_years");
    public static final By SELECT_PERIOD_MONTHS = By.id("duration_months");
    public static final By SELECT_PAYMENT_DATE = By.id("payment_day");
    public static final By BUTTON_NEXT = By.xpath("//*[@id=\"form1\"]/div[2]/button");

    public static final By ERROR_MESSAGE_CONTAINER = By.id("msg-container");
    public static final String ERROR_MESSAGE = "Invalid parameters - please check the marked fields.";
    public static final By NEXT_PAGE_COMPANY_FIELD = By.id("company_name");





}
