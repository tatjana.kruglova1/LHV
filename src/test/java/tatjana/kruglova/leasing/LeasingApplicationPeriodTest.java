package tatjana.kruglova.leasing;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LeasingApplicationPeriodTest {
    public static WebDriver driver = new ChromeDriver();

    @Before
    public void beforeTest() {
        driver.get("https://www.lhv.ee/en/leasing/application");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("iframe")));
        driver.findElement(LeasingApplication.INPUT_DOWNPAYMENT_PERCENTAGE).sendKeys("10");
        driver.findElement(LeasingApplication.INPUT_RESIDUAL_VALUE_PERCENTAGE).sendKeys("10");
        driver.findElement(LeasingApplication.INPUT_VEHICLE_PRICE).sendKeys("10000");
    }

    @AfterClass
    public static void afterClass() {
        driver.quit();
    }

    @Test
    public void testInputNoPeriod() {
        driver.findElement(LeasingApplication.SELECT_PERIOD_YEARS).sendKeys("0");
        driver.findElement(LeasingApplication.SELECT_PERIOD_MONTHS).sendKeys("0");
        assertFalse(driver.findElement(LeasingApplication.BUTTON_NEXT).isEnabled());
    }

    @Test
    public void testInputOneMonthPeriod() {
        driver.findElement(LeasingApplication.SELECT_PERIOD_MONTHS).sendKeys("1");
        assertFalse(driver.findElement(LeasingApplication.BUTTON_NEXT).isEnabled());
    }

    @Test
    public void testInputSixMonthPeriod() {
        driver.findElement(LeasingApplication.SELECT_PERIOD_MONTHS).sendKeys("6");
        driver.findElement(LeasingApplication.BUTTON_NEXT).submit();
        assertTrue(driver.findElement(LeasingApplication.NEXT_PAGE_COMPANY_FIELD).isDisplayed());
    }

    @Test
    public void testInputSixYearPeriod() {
        driver.findElement(LeasingApplication.SELECT_PERIOD_YEARS).sendKeys("6");
        driver.findElement(LeasingApplication.BUTTON_NEXT).submit();
        assertTrue(driver.findElement(LeasingApplication.NEXT_PAGE_COMPANY_FIELD).isDisplayed());
    }

    @Test
    public void testInputMonthsAndYearsPeriod() {
        driver.findElement(LeasingApplication.SELECT_PERIOD_YEARS).sendKeys("4");
        driver.findElement(LeasingApplication.SELECT_PERIOD_MONTHS).sendKeys("5");
        driver.findElement(LeasingApplication.BUTTON_NEXT).submit();
        assertTrue(driver.findElement(LeasingApplication.NEXT_PAGE_COMPANY_FIELD).isDisplayed());
    }


}
