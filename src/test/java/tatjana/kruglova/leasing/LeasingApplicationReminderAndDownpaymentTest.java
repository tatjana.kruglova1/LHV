package tatjana.kruglova.leasing;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

public class LeasingApplicationReminderAndDownpaymentTest {
    public static WebDriver driver = new ChromeDriver();

    @Before
    public void beforeTest() {
        driver.get("https://www.lhv.ee/en/leasing/application");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("iframe")));
        driver.findElement(LeasingApplication.INPUT_VEHICLE_PRICE).sendKeys("10000");
        driver.findElement(LeasingApplication.SELECT_PERIOD_YEARS).sendKeys("1");
    }

    @AfterClass
    public static void afterClass() {
        driver.quit();
    }

    @Test
    public void testUnder100Percent() {
        driver.findElement(LeasingApplication.INPUT_DOWNPAYMENT_PERCENTAGE).sendKeys("10");
        driver.findElement(LeasingApplication.INPUT_RESIDUAL_VALUE_PERCENTAGE).sendKeys("10");
        driver.findElement(LeasingApplication.BUTTON_NEXT).submit();
        assertTrue(driver.findElement(LeasingApplication.NEXT_PAGE_COMPANY_FIELD).isDisplayed());
    }

    @Test
    public void testOver100Percent() {
        driver.findElement(LeasingApplication.INPUT_DOWNPAYMENT_PERCENTAGE).sendKeys("60");
        driver.findElement(LeasingApplication.INPUT_RESIDUAL_VALUE_PERCENTAGE).sendKeys("70");
        driver.findElement(LeasingApplication.BUTTON_NEXT).submit();
        assertTrue(driver.findElement(LeasingApplication.ERROR_MESSAGE_CONTAINER).isDisplayed());
    }

    @Test
    public void test100Percent() {
        driver.findElement(LeasingApplication.INPUT_DOWNPAYMENT_PERCENTAGE).sendKeys("20");
        driver.findElement(LeasingApplication.INPUT_RESIDUAL_VALUE_PERCENTAGE).sendKeys("80");
        driver.findElement(LeasingApplication.BUTTON_NEXT).submit();
        assertTrue(driver.findElement(LeasingApplication.NEXT_PAGE_COMPANY_FIELD).isDisplayed());
    }

}
