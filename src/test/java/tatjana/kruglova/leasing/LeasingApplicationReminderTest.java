package tatjana.kruglova.leasing;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class LeasingApplicationReminderTest {
    public static WebDriver driver = new ChromeDriver();

    @Before
    public void beforeTest() {
        driver.get("https://www.lhv.ee/en/leasing/application");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("iframe")));
        driver.findElement(LeasingApplication.INPUT_VEHICLE_PRICE).sendKeys("10000");
        driver.findElement(LeasingApplication.SELECT_PERIOD_YEARS).sendKeys("1");
        driver.findElement(LeasingApplication.INPUT_DOWNPAYMENT_PERCENTAGE).sendKeys("10");
    }

    @AfterClass
    public static void afterClass() {
        driver.quit();
    }

    @Test
    public void testInputCharsPercentage() {
        driver.findElement(LeasingApplication.INPUT_RESIDUAL_VALUE_PERCENTAGE).sendKeys("asd");
        driver.findElement(LeasingApplication.BUTTON_NEXT).submit();
        assertTrue(driver.findElement(LeasingApplication.ERROR_MESSAGE_CONTAINER).isDisplayed());
        assertEquals(LeasingApplication.ERROR_MESSAGE, driver.findElement(LeasingApplication.ERROR_MESSAGE_CONTAINER).getText());
    }

    @Test
    public void testInputCharsSum() {
        driver.findElement(LeasingApplication.INPUT_RESIDUAL_VALUE_SUM).sendKeys("asd");
        driver.findElement(LeasingApplication.BUTTON_NEXT).submit();
        assertTrue(driver.findElement(LeasingApplication.ERROR_MESSAGE_CONTAINER).isDisplayed());
        assertEquals(LeasingApplication.ERROR_MESSAGE, driver.findElement(LeasingApplication.ERROR_MESSAGE_CONTAINER).getText());
    }

    @Test
    public void testInput5Percentage() {
        driver.findElement(LeasingApplication.INPUT_RESIDUAL_VALUE_PERCENTAGE).sendKeys("5");
        String sum = driver.findElement(LeasingApplication.INPUT_RESIDUAL_VALUE_SUM).getAttribute("value");
        assertEquals("500.00", sum);
    }

    @Test
    public void testInput1000Sum() {
        driver.findElement(LeasingApplication.INPUT_RESIDUAL_VALUE_SUM).sendKeys("1000");
        String sum = driver.findElement(LeasingApplication.INPUT_RESIDUAL_VALUE_PERCENTAGE).getAttribute("value");
        assertEquals("10.00", sum);
    }

    @Test
    public void test0Sum() {
        driver.findElement(LeasingApplication.INPUT_RESIDUAL_VALUE_SUM).sendKeys("0");
        driver.findElement(LeasingApplication.BUTTON_NEXT).submit();
        assertTrue(driver.findElement(LeasingApplication.NEXT_PAGE_COMPANY_FIELD).isDisplayed());
    }

    @Test
    public void test0Percent() {
        driver.findElement(LeasingApplication.INPUT_RESIDUAL_VALUE_PERCENTAGE).sendKeys("0");
        driver.findElement(LeasingApplication.BUTTON_NEXT).submit();
        assertTrue(driver.findElement(LeasingApplication.NEXT_PAGE_COMPANY_FIELD).isDisplayed());
    }

    @Test
    public void test1Sum() {
        driver.findElement(LeasingApplication.INPUT_RESIDUAL_VALUE_SUM).sendKeys("1");
        driver.findElement(LeasingApplication.BUTTON_NEXT).submit();
        assertTrue(driver.findElement(LeasingApplication.NEXT_PAGE_COMPANY_FIELD).isDisplayed());
    }

    @Test
    public void test1Percent() {
        driver.findElement(LeasingApplication.INPUT_RESIDUAL_VALUE_PERCENTAGE).sendKeys("1");
        driver.findElement(LeasingApplication.BUTTON_NEXT).submit();
        assertTrue(driver.findElement(LeasingApplication.NEXT_PAGE_COMPANY_FIELD).isDisplayed());
    }

    @Test
    public void testMinus500Sum() {
        driver.findElement(LeasingApplication.INPUT_RESIDUAL_VALUE_SUM).sendKeys("-500");
        driver.findElement(LeasingApplication.BUTTON_NEXT).submit();
        assertTrue(driver.findElement(LeasingApplication.ERROR_MESSAGE_CONTAINER).isDisplayed());
        assertEquals(LeasingApplication.ERROR_MESSAGE, driver.findElement(LeasingApplication.ERROR_MESSAGE_CONTAINER).getText());
    }

    @Test
    public void test10000Sum() {
        driver.findElement(LeasingApplication.INPUT_RESIDUAL_VALUE_SUM).sendKeys("10000");
        driver.findElement(LeasingApplication.BUTTON_NEXT).submit();
        assertTrue(driver.findElement(LeasingApplication.ERROR_MESSAGE_CONTAINER).isDisplayed());
        assertEquals(LeasingApplication.ERROR_MESSAGE, driver.findElement(LeasingApplication.ERROR_MESSAGE_CONTAINER).getText());
    }

    @Test
    public void test12000Sum() {
        driver.findElement(LeasingApplication.INPUT_RESIDUAL_VALUE_SUM).sendKeys("12000");
        driver.findElement(LeasingApplication.BUTTON_NEXT).submit();
        assertTrue(driver.findElement(LeasingApplication.ERROR_MESSAGE_CONTAINER).isDisplayed());
        assertTrue(driver.findElement(LeasingApplication.ERROR_MESSAGE_CONTAINER).getText().contains(LeasingApplication.ERROR_MESSAGE));
    }

    @Test
    public void test100Percent() {
        driver.findElement(LeasingApplication.INPUT_RESIDUAL_VALUE_PERCENTAGE).sendKeys("100");
        driver.findElement(LeasingApplication.BUTTON_NEXT).submit();
        assertTrue(driver.findElement(LeasingApplication.ERROR_MESSAGE_CONTAINER).isDisplayed());
        assertEquals(LeasingApplication.ERROR_MESSAGE, driver.findElement(LeasingApplication.ERROR_MESSAGE_CONTAINER).getText());
    }

    @Test
    public void testMinus55Percent() {
        driver.findElement(LeasingApplication.INPUT_RESIDUAL_VALUE_PERCENTAGE).sendKeys("-55");
        driver.findElement(LeasingApplication.BUTTON_NEXT).submit();
        assertTrue(driver.findElement(LeasingApplication.ERROR_MESSAGE_CONTAINER).isDisplayed());
        assertEquals(LeasingApplication.ERROR_MESSAGE, driver.findElement(LeasingApplication.ERROR_MESSAGE_CONTAINER).getText());
    }

    @Test
    public void test101Percent() {
        driver.findElement(LeasingApplication.INPUT_RESIDUAL_VALUE_PERCENTAGE).sendKeys("101");
        driver.findElement(LeasingApplication.BUTTON_NEXT).submit();
        assertTrue(driver.findElement(LeasingApplication.ERROR_MESSAGE_CONTAINER).isDisplayed());
        assertTrue(driver.findElement(LeasingApplication.ERROR_MESSAGE_CONTAINER).getText().contains(LeasingApplication.ERROR_MESSAGE));
    }


}
