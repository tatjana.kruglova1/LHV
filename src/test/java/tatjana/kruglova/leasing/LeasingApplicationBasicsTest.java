package tatjana.kruglova.leasing;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class LeasingApplicationBasicsTest {
    public static WebDriver driver = new ChromeDriver();

    @Before
    public void beforeTest() {
        driver.get("https://www.lhv.ee/en/leasing/application");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("iframe")));
    }

    @AfterClass
    public static void afterClass() {
        driver.quit();
    }

    @Test
    public void testPageOpening() {
        String pageTitle = driver.getTitle();
        assertTrue(pageTitle.contains("Leasing application"));
    }

    @Test
    public void testEmptyForm() {
        assertFalse(driver.findElement(LeasingApplication.BUTTON_NEXT).isEnabled());
    }

    @Test
    public void testDefaultLeasingPerson() {
        assertTrue(driver.findElement(LeasingApplication.RADIO_BUTTON_PRIVATE_PERSON).isSelected());
    }

    @Test
    public void testSelectLeasingPerson() {
        driver.findElement(LeasingApplication.RADIO_BUTTON_LEGAL_PERSON).click();
        assertTrue(driver.findElement(LeasingApplication.RADIO_BUTTON_LEGAL_PERSON).isSelected());
    }

    @Test
    public void testDefaultLeaseType() {
        assertTrue(driver.findElement(LeasingApplication.RADIO_BUTTON_FINANTIAL_LEASE).isSelected());
    }

    @Test
    public void testSelectLeaseType() {
        driver.findElement(LeasingApplication.RADIO_BUTTON_OPERATING_LEASE).click();
        assertTrue(driver.findElement(LeasingApplication.RADIO_BUTTON_OPERATING_LEASE).isSelected());
    }

    @Test
    public void testDefaultVATCheckbox() {
        assertFalse(driver.findElement(LeasingApplication.CHECKBOX_VAT_VALUE).isSelected());
    }

    @Test
    public void testCheckVATCheckbox() {
        driver.findElement(LeasingApplication.CHECKBOX_VAT_VALUE).click();
        assertTrue(driver.findElement(LeasingApplication.CHECKBOX_VAT_VALUE).isSelected());
    }

    @Test
    public void testInputPaymentDate5() {
        driver.findElement(LeasingApplication.SELECT_PAYMENT_DATE).sendKeys("5");
        assertEquals("5",driver.findElement(LeasingApplication.SELECT_PAYMENT_DATE).getAttribute("value"));
    }

    @Test
    public void testInputPaymentDate15() {
        driver.findElement(LeasingApplication.SELECT_PAYMENT_DATE).sendKeys("15");
        assertEquals("15",driver.findElement(LeasingApplication.SELECT_PAYMENT_DATE).getAttribute("value"));
    }

    @Test
    public void testInputPaymentDate25() {
        driver.findElement(LeasingApplication.SELECT_PAYMENT_DATE).sendKeys("25");
        assertEquals("25",driver.findElement(LeasingApplication.SELECT_PAYMENT_DATE).getAttribute("value"));
    }

    @Test
    public void testInputPaymentDate10() {
        driver.findElement(LeasingApplication.SELECT_PAYMENT_DATE).sendKeys("10");
        assertNotSame("10",driver.findElement(LeasingApplication.SELECT_PAYMENT_DATE).getAttribute("value"));
    }

    @Test
    public void testInputPaymentDate0() {
        driver.findElement(LeasingApplication.SELECT_PAYMENT_DATE).sendKeys("0");
        assertNotSame("0",driver.findElement(LeasingApplication.SELECT_PAYMENT_DATE).getAttribute("value"));
    }
}
